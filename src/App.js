import React, { useEffect, useState } from "react";
import axios from "axios";

export const App = () => {
  const [countries, setCountries] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    axios
      .get("/api/v1/goldprice_website")
      .then((res) => {
        setCountries(res.data);
        setLoad(true);
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  if (load) {
    return (
      <ul>
        {error ? <li>{error.message}</li> : <h1>{countries.G965B.bid}</h1>}
      </ul>
    );
  } else {
    return <div>Loading...</div>;
  }
};

export default App;
